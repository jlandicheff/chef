# Chef4Team Challenge

## Participation

La participation est ouverte à tous les employés de Tech4team (à date de clôture des inscriptions - à déterminer.)

## Termes du challenge

### Objectifs

Les participants doivent présenter, pour tous les membres du jury et eux-mêmes, les éléments suivants:

* Entrée ou amuse-bouche
* Plat principal
* Dessert
* Boisson (apéritif et/ou accompagnant le plat ou dessert)

### Moyens à disposition

L'inventaire des moyens mis à disposition (équipement, outils, vaisselle, etc.) sera communiqué à l'ensemble des participants.

Les participants désireux d'utiliser des moyens non présents dans cette liste devront les faire accepter par l'ensemble des autres candidats.

Les participants devront apporter leur propre tablier.

### Temps

Les participants pourront se voir allouer du temps de préparation à la veille de leur tour dans la compétition.
// TODO à préciser

### Dates et heures des compétitions

A déterminer.

### Notation

A l'issu de chaque repas, les participants (à l'exception du cuisinier) délivreront une note secrète prenant en compte les éléments suivants:

* Repas complet
* Budget alloué par personne (sans prendre en compte la boisson)
Le cuisinier devra donc présenter les preuves d'achat correspondantes
* Respect des contraintes et bonne foi :)

Les notes seront révélées à l'issu de la compétition.  
Le gagnant remportera le titre honorifique de "Chef Tech4Team"
